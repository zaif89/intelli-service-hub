# Intelli Service Hub

This repository has code for the Tickets Detail page of Intelli Service Hub. 
 
[Screenshot](https://prnt.sc/26lxc8h)

## Local Development Versions
##### NPM: 8.3.2
##### Node:  16.13.2
##### React: ^17.0.2

## Installation

Using GIT to install the project

```bash
git clone https://zaif89@bitbucket.org/zaif89/intelli-service-hub.git
cd intelli-service-hub
npm install
```

For REST Apis, we are using **json-server**. Use the following command to start it, inside the **intelli-service-hub** directory
```bash
npx json-server -p 8000 -w data/db.json
```
Setup environment files
```bash
cp .env.example .env
```
To run the React app
```bash
npm run start
```
## i18n
i18n has been implemented using **react-intl** and **context api**. You can choose from English/French locales from the dropdown menu in header. [Screenshot](https://prnt.sc/26lxegm)

## a11y
The frontend is written with the help of **Ant Design**, a React UI library which has built-in support for a11y. It adds all the necessary **aria** attributes in the rendered controls. Here are some more details on [github](https://github.com/ant-design/ant-design/search?q=aria&type=Code&utf8=%E2%9C%93) and [medium](https://medium.com/@spsaucier/reflections-on-ant-design-after-using-it-for-two-years-85fae13e8186)

## Storybook
To check the storybook integration, use:
```bash
npm run storybook
```
Currently, there is one story created for Ticket Card UI. [Screenshot](https://prnt.sc/26lxe72)