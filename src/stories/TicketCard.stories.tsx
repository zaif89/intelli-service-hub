import React from "react";
import { ComponentStory, ComponentMeta } from '@storybook/react';
import Ticket from "../components/Ticket/TicketCard";

import 'antd/dist/antd.min.css';
import 'antd-css-utilities/utility.min.css';
import '../style.css';

export default {
    title: "Ticket",
    component: Ticket
} as ComponentMeta<typeof Ticket>;

const Template: ComponentStory<typeof Ticket> = (args) => {
    return (
        <div style={{border: "1px solid #CCC"}}>
            <Ticket {...args} />;
        </div>
    )
}

export const TicketCard = Template.bind({});

TicketCard.args = {
    heading: "Ticket title here",
    subheading: "sub heading text",
    timestamp: new Date("2022-01-26 22:00"),
    description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
};