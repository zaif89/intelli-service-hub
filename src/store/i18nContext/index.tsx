import { createContext, useState } from "react";
import { LOCALES } from "../../components/i18n";

const I18nContext = createContext({
    locale: LOCALES.ENGLISH,
    setLanguage: (locale: string) => {},
});

interface ContextProviderProps {
    children: any
}

const I18nContextProvider = (props: ContextProviderProps) => {

    const { children } = props;

    const [defaultLocale, setDefaultLocale] = useState(LOCALES.ENGLISH);

    const setDefaultLanguage = (locale: string) => {
        setDefaultLocale(locale);
    }

    const contextValue = {
        locale: defaultLocale,
        setLanguage: setDefaultLanguage 
    }

    return (
        <I18nContext.Provider value={contextValue}>
            { children }
        </I18nContext.Provider>
    )
}

export { I18nContext, I18nContextProvider }