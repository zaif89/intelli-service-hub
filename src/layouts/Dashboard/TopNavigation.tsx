import React, { useEffect, useState, useContext } from "react";
import { Col, Layout, Row, Input, Button, Menu, Badge, Avatar, Select } from "antd";
import { 
    SearchOutlined,
    DownOutlined,
    MailOutlined,
    BellOutlined,
    UserOutlined
} from "@ant-design/icons";
import UserService from "../../services/UserService";
import { AUTH_USER_ID } from "../../constants";
import { I18nContext } from "../../store/i18nContext";
import { LOCALES } from "../../components/i18n";
import translate from "../../components/i18n/translate";

const TopNavigation = () => {

    const { Header } = Layout;

    const [currentUser, setCurrentUser] = useState<any>(null);

    const i18nCtx = useContext(I18nContext);
    
    const { Option } = Select;

    const handleLanguageChange = (language: string) => {
        i18nCtx.setLanguage(language)
    }

    useEffect(() => {
        UserService.getUserDetails(AUTH_USER_ID)
            .then(response => {
                if( response.status === 200 ) {
                    setCurrentUser(response.data)
                }
            })
            .catch(error => {
                console.log(error)
            })
    }, []);

    return (
        <Header className="primaryBg" style={{ borderBottom: "1px solid #dcdcdc" }}>
            <Row>
                <Col span={8}>
                    <Row gutter={8}>
                        <Col span={12}>
                            <Input placeholder="Search..." prefix={<SearchOutlined />} />
                        </Col>
                        <Col span={12}>
                            <Select placeholder="Select Language" className="w-100" onChange={(value) => handleLanguageChange(value)}>
                                <Option value={LOCALES.ENGLISH}>English</Option>
                                <Option value={LOCALES.FRENCH}>FRENCH</Option>
                            </Select>
                        </Col>
                    </Row>
                </Col>
                <Col offset={4} span={12}>
                    <Row>
                        <Col span={8}>
                            <Button shape="round" type="primary" className="primaryBtn">
                                { translate('navigation.addBtn') } <DownOutlined size={1} />
                            </Button>
                        </Col>
                        <Col span={16}>
                            {
                                currentUser && (
                                    <Menu mode="horizontal" className="header-navigation">
                                        <Menu.Item key="messages">
                                            <Badge count={currentUser.unread.messages} style={{ backgroundColor: '#52c41a'}}>
                                                <Avatar className="primaryBg" shape="square" icon={<MailOutlined />} />
                                            </Badge>
                                        </Menu.Item>
                                        <Menu.Item key="notifications">
                                            <Badge count={currentUser.unread.notifications}>
                                                <Avatar className="primaryBg" shape="square" icon={<BellOutlined />} />
                                            </Badge>
                                        </Menu.Item>
                                        <Menu.Item key="profile">
                                            <Avatar className="mr-2" shape="circle" icon={<UserOutlined/>} />
                                            <label>{ currentUser.first_name} {currentUser.last_name }<DownOutlined /></label>
                                        </Menu.Item>
                                    </Menu>
                                )
                            }
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Header>
    )
}

export default TopNavigation;