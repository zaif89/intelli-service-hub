import React, { useState } from "react";
import { Layout, Menu } from 'antd';
import {
    UserOutlined,
    HomeOutlined,
    CreditCardOutlined,
    MessageOutlined,
    MailOutlined,
    LineChartOutlined,
    AppstoreOutlined
} from '@ant-design/icons';
import Avatar from "antd/lib/avatar/avatar";
import translate from "../../components/i18n/translate";

const Sidebar = () => {

    const { Sider } = Layout;

    const [collapsed, setCollapsed] = useState(true);

    const onCollapse = () => {
        setCollapsed(!collapsed);
    };

    return (
        <Sider style={{ borderRight: "1px solid #dcdcdc" }} collapsible={true} collapsed={collapsed} onCollapse={onCollapse} theme='light'>
            <div className="logo-wrapper">
                <Avatar className="logo" size="large" icon={<MailOutlined/>}/>
            </div>
            <Menu theme="light" defaultSelectedKeys={['2']} mode="inline">
                <Menu.Item key="1" icon={<HomeOutlined />}>
                    { translate('sidebar.home') }
                </Menu.Item>
                <Menu.Item key="2" icon={<CreditCardOutlined />}>
                    { translate('sidebar.tickets') }
                </Menu.Item>
                <Menu.Item key="3" icon={<UserOutlined />}>
                    { translate('sidebar.customers') }
                </Menu.Item>
                <Menu.Item key="4" icon={<AppstoreOutlined />}>
                    { translate('sidebar.customers') }
                </Menu.Item>
                <Menu.Item key="5" icon={<MessageOutlined />}>
                    { translate('sidebar.messages') }
                </Menu.Item>
                <Menu.Item key="6" icon={<LineChartOutlined />}>
                    { translate('sidebar.analytics') }
                </Menu.Item>
            </Menu>
        </Sider>
    );

}

export default Sidebar;