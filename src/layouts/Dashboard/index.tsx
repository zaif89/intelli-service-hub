import React from 'react';
import { Layout } from 'antd';
import { DashboardProps } from './Dashboard';
import Sidebar from './Sidebar';
import TopNavigation from './TopNavigation';

const { Content } = Layout;

const Dashboard = (props: DashboardProps) => {

    const { children } = props;

    return (
        <Layout className='h-100vh'>
            <Sidebar />
            <Layout className="site-layout">
                <TopNavigation />
                <Content className="ma-0 overflow-x-hidden">
                    { children }
                </Content>
            </Layout>
        </Layout>
    );
}

export default Dashboard;