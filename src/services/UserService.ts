import { AxiosPromise } from 'axios';
import API from "../config/axios";

class UserService {

    /**
     * User role dashboard counts
     * @returns AxiosPromise
     */
    static getUser = (): AxiosPromise => {
        return API.get("users");
    }

    /**
     * User detail api
     * @returns AxiosPromise
     */
    static getUserDetails = (id: number): AxiosPromise => {
        return API.get(`users/${id}`);
    };
}

export default UserService;
