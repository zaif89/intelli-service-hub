import { AxiosPromise } from 'axios';
import API from "../config/axios";

class TicketService {

    /**
     * User role dashboard counts
     * @returns AxiosPromise
     */
    static getTickets = (): AxiosPromise => {
        return API.get("tickets");
    }

    /**
     * User detail api
     * @returns AxiosPromise
     */
    static getTicketDetails = (id: number): AxiosPromise => {
        return API.get(`tickets/${id}`);
    };
}

export default TicketService;
