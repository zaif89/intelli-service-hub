import React from "react";
import { Button, Select, Typography } from "antd";
import translate from "../../../components/i18n/translate";

const Filters = () => {

    const { Title } = Typography;

    return (
        <div className='filters-wrap pa-3 h-100vh'>
            <Title className="my-5" level={4}>{ translate('filters.title') }</Title>
            <div className="mb-10">
                <p><label>{translate('filters.type')}</label></p>
                <Select id="questions" placeholder="Questions" bordered={false} className="customSelect"/>
            </div>
            <div className="mb-10">
                <p><label>{translate('filters.status')}*</label></p>
                <Select id="questions" placeholder="Open" bordered={false} className="customSelect"/>
            </div>
            <div className="mb-10">
                <p><label>{translate('filters.priority')}</label></p>
                <Select id="questions" placeholder="Medium" bordered={false} className="customSelect"/>
            </div>
            <div className="mb-10">
                <p><label>{translate('filters.assigned')}</label></p>
                <Select id="questions" placeholder="Escalations" bordered={false} className="customSelect"/>
            </div>

            <div>
                <Button shape="round" className="primaryBtn">{ translate('filters.update') }</Button>
            </div>
        </div>
    );
}

export default Filters;