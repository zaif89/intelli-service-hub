import React from "react";
import { Breadcrumb, Button } from "antd";
import { 
    MailOutlined, 
    StarOutlined,
    DashOutlined,
    RollbackOutlined,
    FormOutlined,
    SendOutlined,
    CheckCircleOutlined,
    DeleteOutlined,
    BranchesOutlined
} from "@ant-design/icons";
import translate from "../../../components/i18n/translate";

const TicketActions = () => {
    return (
        <>
            <Breadcrumb className="pb-5">
                <Breadcrumb.Item>{ translate('ticket.pageTitle') }</Breadcrumb.Item>
                <Breadcrumb.Item>
                24 <MailOutlined size={5} />
                </Breadcrumb.Item>
                <Breadcrumb.Item>Overdue</Breadcrumb.Item>
            </Breadcrumb>

            <div className="button-row pb-5">
                <Button size="small" className="mr-2" icon={<StarOutlined />} />
                <Button size="small" icon={<RollbackOutlined className="mr-1" />}>
                    { translate('ticket.actions.reply') }
                </Button>
                <Button size="small" icon={<FormOutlined className="mr-1"/>}>
                    { translate('ticket.actions.addNote') }
                </Button>
                <Button size="small" icon={<SendOutlined className="mr-1"/>}>
                    { translate('ticket.actions.forward') }
                </Button>
                <Button size="small" icon={<CheckCircleOutlined className="mr-1"/>}>
                    { translate('ticket.actions.close') }
                </Button>
                <Button size="small" icon={<BranchesOutlined className="mr-1"/>}>
                    { translate('ticket.actions.merge') }
                </Button>
                <Button size="small" icon={<DeleteOutlined className="mr-1"/>}>
                    { translate('ticket.actions.delete') }
                </Button>
                <Button size="small" className="ml-2" icon={<DashOutlined className="dotted-menu"/>} />
            </div>
        </>
    )
}

export default TicketActions;