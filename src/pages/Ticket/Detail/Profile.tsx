import React, { useEffect, useState } from "react";
import { Avatar, Collapse, Typography } from "antd";
import { 
    UserOutlined,
    LoadingOutlined 
} from "@ant-design/icons";
import UserService from "../../../services/UserService";
import { CUSTOMER_ID } from "../../../constants";
import translate from "../../../components/i18n/translate";

const Profile = () => {

    const { Title, Text } = Typography;

    const { Panel } = Collapse;

    const [user, setUser] = useState<any>(null);

    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        UserService.getUserDetails(CUSTOMER_ID)
            .then(response => {
                if( response.status === 200 ) {
                    setUser(response.data)
                    setIsLoading(false)
                }
            })
            .catch(error => {
                console.log(console.log(error))
                setIsLoading(false)
            })
    }, [])
    
    return (
        <div className='filters-wrap pa-3 h-100vh'>
            <Title className="my-5" level={4}>
                { translate('profile.title') }
            </Title>
            
            { isLoading && <LoadingOutlined /> }
            
            { user && (
                <>
                    <div>
                        <Avatar icon={<UserOutlined />} shape="square" size={50} />
                        <Text type="secondary" className="pa-1">{user.first_name} {user.last_name}</Text>
                    </div>
                    <div className="mt-10">
                        <p><label>{translate('profile.email')}</label></p>
                        <Text type="secondary">{user.email}</Text>
                    </div>
                    <div className="my-10">
                        <p><label>{translate('profile.contact')}</label></p>
                        <Text type="secondary">{user.contact_number}</Text>
                    </div>
                </>
            )}

            <Collapse
                defaultActiveKey={['1']}
                expandIconPosition="right"
                ghost
            >
                <Panel className="py-3" style={{ borderTop: "1px solid #dcdcdc" }} header={<Text strong>{translate('profile.timeLogs')}</Text>} key="2">
                    <div>Lorem Ipsum</div>
                </Panel>

                <Panel className="py-3" style={{ borderTop: "1px solid #dcdcdc" }} header={<Text strong>{translate('profile.toDo')}</Text>} key="3">
                    <div>Lorem Ipsum</div>
                </Panel>
            </Collapse>
        </div>
    );
}

export default Profile;