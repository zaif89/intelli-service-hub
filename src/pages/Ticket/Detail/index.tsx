import React from "react";
import { Row, Col} from "antd";
import Ticket from "../../../components/Ticket";
import { Typography } from "antd";
import TicketActions from "./TicketActions";
import Filters from "./Filters";
import Profile from "./Profile";
import { TICKET_ID } from "../../../constants";
import translate from "../../../components/i18n/translate";

const TicketDetail = () => {

    const { Title } = Typography;

    return (
        <>
            <Row gutter={16}>
                <Col sm={24} md={24} lg={14}>
                    <div className="pa-10">
                        <Title level={4}>{ translate('ticket.pageTitle') }</Title>
                        <TicketActions />
                        <Ticket ticketId={TICKET_ID}/>
                    </div>
                </Col>
                <Col sm={24} md={24} lg={10} className="primaryBg h-100vh w-100" style={{ right: 0 }}>
                    <Row>
                        <Col span={12}>
                            <Filters />
                        </Col>
                        <Col span={12}>
                            <Profile />
                        </Col>
                    </Row>
                </Col>
            </Row>
        </>
    )
}

export default TicketDetail;