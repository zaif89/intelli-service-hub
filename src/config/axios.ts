import axios from "axios";
import { API_URL } from "../constants";

const defaultOptions = {
    baseURL: API_URL,
    headers: {
        'Content-Type': 'application/json'
    },
};

// Create instance
let instance = axios.create(defaultOptions);

// Add a request interceptor
instance.interceptors.request.use(
    function (config) {
        // Set auth token before request is sent
        return config;
    },
    function (error) {
        // Do something with request error
        return Promise.reject(error);
    }
);

// Add a response interceptor
instance.interceptors.response.use(
    function (response) {
        // Do something with response data
        return response;
    },
    function (error) {

        if( error.response.status === 404 ) {
            // Not found
        }

        return Promise.reject(error);
    }
);

export default instance;
