export const API_URL   = process.env.REACT_APP_API_BASE;
export const APP_URL   = process.env.REACT_APP_URL;
export const APP_NAME  = process.env.REACT_APP_NAME; 

export const AUTH_USER_ID = 1;
export const CUSTOMER_ID  = 2;
export const TICKET_ID    = 1;