import React, { useContext } from 'react';
import TicketDetail from './pages/Ticket/Detail';
import Dashboard from './layouts/Dashboard';
import Provider from './components/i18n/Provider';
import { I18nContext } from './store/i18nContext';
import { BrowserRouter, Navigate, Routes, Route } from 'react-router-dom';

const App = () => {

  const i18nCtx = useContext(I18nContext);

  return (
    <Provider locale={i18nCtx.locale}>
      <Dashboard>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<TicketDetail />}/>
            <Route path="*" element={<Navigate to="/" />}/>
          </Routes>
        </BrowserRouter>
      </Dashboard>
    </Provider>
  );
}

export default App;
