import React from "react";
import { Row, Col, Typography } from "antd";
import { UserOutlined } from "@ant-design/icons"
import Avatar from "antd/lib/avatar/avatar";

interface TicketTitleInterface {
    heading: string,
    subheading: string
}

const TicketTitle = (props: TicketTitleInterface) => {
    
    const { Title, Text } = Typography;
    const { heading, subheading } = props;

    return (
        <>
            <Row gutter={6}>
                <Col className="pt-1">
                    <Avatar size={40}><UserOutlined /></Avatar>
                </Col>
                <Col>
                    <Title level={5}>{heading}</Title>
                    <Text type="secondary" strong>{subheading}</Text>
                </Col>
            </Row>
        </>
    );
}

export default TicketTitle;