export interface TicketCardProps {
    heading: string,
    subheading: string,
    description: string,
    timestamp: Date
}