import React from "react";
import { Card } from "antd";
import TicketTitle from "../TicketTitle";
import moment from "moment";
import { Typography } from "antd";
import { DashOutlined } from "@ant-design/icons";
import { TicketCardProps } from "./TicketCard";

const TicketCard = (props: TicketCardProps) => {

    const { heading, subheading, timestamp, description } = props;

    const { Paragraph, Text } = Typography;

    const title = <TicketTitle heading={ heading } subheading={subheading}/>

    const get_timestamp_string = () => {
        const parsed_time = moment(timestamp);    
        const time_ago = parsed_time.toNow(true)
        const date_time = parsed_time.format("ddd DD YYYY hh:mm A") 
        return `${time_ago} ago (${date_time})`
    }

    const extra = get_timestamp_string()
    
    return (
        <>
            <Card className="mb-10" 
                extra={<Text strong type="secondary">{extra} <DashOutlined className="dotted-menu"/></Text>} 
                title={title} 
                bordered={false}
                type="inner"
            >
                <Paragraph type="secondary">
                    {description}
                </Paragraph>
            </Card>
        </>
    );
}

export default TicketCard;