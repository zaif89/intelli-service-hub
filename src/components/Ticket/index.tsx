import React, { useState, useEffect } from "react";
import { LoadingOutlined } from '@ant-design/icons';
import TicketService from "../../services/TicketService";
import TicketCard from "./TicketCard";

interface TicketProps {
    ticketId: number;
}

const Ticket = (props: TicketProps) => {

    const { ticketId } = props;

    const [ticket, setTicket] = useState<any>(null);

    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        TicketService.getTicketDetails(ticketId)
            .then(response => {
                if( response.status === 200 ) {
                    setTicket(response.data)
                    setIsLoading(false)
                }
            }).catch(error => {
                setIsLoading(false)
            })
        return () => {}
    }, [ticketId]);

    return (
        <>
            { isLoading && <LoadingOutlined /> }

            { ticket && (
                <TicketCard 
                    heading={ticket.title} 
                    subheading={`${ticket.full_name} ${ticket.label}`}
                    description={ticket.description}
                    timestamp={ticket.created_at}
                    {...ticket}
                />
            )}

            { ticket && ticket.replies && (
                ticket.replies.map((reply: any, key: number) => (
                    <TicketCard
                        key={key}
                        heading={ reply.title }
                        subheading={ `${reply.label} ${ticket.full_name}` }
                        description={ reply.description }
                        timestamp={ reply.created_at }
                        {...reply}
                    />
                ))
            )}
        </>
    );
}

export default Ticket;