import { Fragment } from "react";
import { IntlProvider } from 'react-intl';
import messages from "./messages";
import { LOCALES } from './constants';

import flatten from "flat";

interface ProviderProps {
    children: any,
    locale: string
}

const Provider = (props: ProviderProps) => {

    const { children, locale = LOCALES.ENGLISH } = props;

    return (
        <IntlProvider
            textComponent={Fragment} locale={locale}
            messages={flatten(messages[locale])}
        >
            {children}
        </IntlProvider>
    )

}

export default Provider;