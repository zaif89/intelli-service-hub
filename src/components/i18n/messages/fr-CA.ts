import { LOCALES } from "../constants";

const fr = {
    [LOCALES.FRENCH]: {
        sidebar: {
            home: "Accueil",
            tickets: "Des billets",
            customers: "Clients",
            messages: "Messages",
            analytics: "Analytique"
        },
        navigation: {
            search: "Cherche ici",
            language: "Choisir la langue",
            addBtn: "Nouveau billet"
        },
        ticket: {
            pageTitle: "Tous les billets",
            actions: {
                reply: "Répondre",
                addNote: "Noter",
                forward: "Effronté",
                close: "Fermer",
                merge: "Fusionner",
                delete: "Supprimer"
            }
        },
        filters: {
            title: "Propriétés",
            type: "Taper",
            questions: "Des questions",
            status: "Statut",
            open: "Ouvert",
            priority: "Priorité",
            medium: "Moyen",
            assigned: "Attribué",
            escalation: "Escalades",
            update: "Mettre à jour"
        },
        profile: {
            title: "Détails du contact",
            email: "Email",
            contact: "Téléphone de travail",
            timeLogs: "Journaux de temps",
            toDo: "Faire"
        }
    }
}

export default fr;