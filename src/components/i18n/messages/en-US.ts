import { LOCALES } from "../constants";

const en =  {
    [LOCALES.ENGLISH]: {
        sidebar: {
            home: "Home",
            tickets: "Tickets",
            customers: "Customers",
            messages: "Messages",
            analytics: "Analytics"
        },
        navigation: {
            search: "Search Here",
            language: "Select Language",
            addBtn: "New Ticket"
        },
        ticket: {
            pageTitle: "All Tickets",
            actions: {
                reply: "Reply",
                addNote: "Add Note",
                forward: "Forward",
                close: "Close",
                merge: "Merge",
                delete: "Delete"
            }
        },
        filters: {
            title: "Properties",
            type: "Type",
            questions: "Questions",
            status: "Status",
            open: "Open",
            priority: "Priority",
            medium: "Medium",
            assigned: "Assigned To",
            escalation: "Escalations",
            update: "Update"
        },
        profile: {
            title: "Contact Details",
            email: "Email ID",
            contact: "Work Phone",
            timeLogs: "Time Logs",
            toDo: "To Do"
        }
    } 
}

export default en;